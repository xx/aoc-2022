import std/rdstdin
import std/sets
import std/sequtils
import strutils

var elf_sets = newSeq[HashSet[int]](2)
var duplicates: int
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok:
    break
  let elves = line.split(',')
  for i in 0..1:
    let bounds = elves[i].split('-')
    elf_sets[i] = toHashSet(toSeq(parseInt(bounds[0]) .. parseInt(bounds[1])))
  if (elf_sets[0] * elf_sets[1]).card != 0:
    duplicates.inc

echo(duplicates)
