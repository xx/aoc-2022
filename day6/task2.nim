import std/deques
import std/sets
import std/sequtils
import std/rdstdin

const UNIQ_COUNT = 14

var deq = initDeque[char]()
let input = readLineFromStdin("")

for i in 0 .. UNIQ_COUNT - 2:
  deq.addLast(input[i])

for i in UNIQ_COUNT - 1 .. input.len - 1:
  deq.addLast(input[i])
  if deq.toSeq.toHashSet.card == UNIQ_COUNT:
    echo(i + 1)
    break
  deq.popFirst
