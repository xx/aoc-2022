import std/rdstdin
import std/algorithm
import std/strutils
import std/sequtils

const STACKS = 9

var stacks = newSeq[string](STACKS)
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok: break
  if line[1] == '1':
    discard readLineFromStdin("", line)
    break
  for i in 0 .. STACKS - 1:
    if line[i * 4] == ' ':
      continue
    stacks[i].add(line[i * 4 + 1])

for i in 0 .. STACKS - 1:
  stacks[i].reverse

while true:
  let ok = readLineFromStdin("", line)
  if not ok: break
  let command = line.split(' ')
  let amount = parseInt(command[1])
  let fr = parseInt(command[3]) - 1
  let to = parseInt(command[5]) - 1
  let fr_len = stacks[fr].len - 1
  stacks[to] = stacks[to] & stacks[fr][fr_len - amount + 1 .. ^1]
  stacks[fr].delete(fr_len - amount + 1, fr_len)

for i in 0 .. STACKS - 1:
  stdout.write(stacks[i][stacks[i].len - 1])
stdout.flushFile
