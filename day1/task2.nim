import std/rdstdin
import strutils
import std/algorithm

var cal_counts = newSeq[int](0)
var cals: int
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok:
    cal_counts.add(cals)
    break
  if line.len == 0:
    cal_counts.add(cals)
    cals = 0
  else:
    cals += parseInt(line)

cal_counts.sort(order = SortOrder.Descending)

echo(cal_counts[0] + cal_counts[1] + cal_counts[2])
