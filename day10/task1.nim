import system/io
import strutils

var ip, strength: int
var reg_x = 1
var read_mode: bool
var read_dest: ptr int
let dump_regs = [20, 60, 100, 140, 180, 220] # cycle when to calculate strength

let instructions = stdin.readAll.split

while true:
  if ip == instructions.len - 1: break

  if read_mode:
    read_dest[] += parseInt(instructions[ip])
    read_mode = false
  else:
    case instructions[ip]:
      of "noop": discard
      of "addx":
        read_mode = true
        read_dest = addr(reg_x)

  if ip + 2 in dump_regs:
    strength += reg_x * (ip + 2)

  ip.inc

echo strength
