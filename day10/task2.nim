import system/io
import strutils

const CRT_WIDTH = 40

var ip: int
var reg_x = 1
var read_mode: bool
var read_dest: ptr int

let instructions = stdin.readAll.split

while true:
  if ip == instructions.len - 1: break

  # draw CRT
  let crt_v = ip mod CRT_WIDTH

  if crt_v > reg_x - 2 and crt_v < reg_x + 2:
    stdout.write("#")
  else:
    stdout.write(".")

  if crt_v == CRT_WIDTH - 1:
    echo ""

  # process instructions
  if read_mode:
    read_dest[] += parseInt(instructions[ip])
    read_mode = false
  else:
    case instructions[ip]:
      of "noop": discard
      of "addx":
        read_mode = true
        read_dest = addr(reg_x)

  ip.inc
