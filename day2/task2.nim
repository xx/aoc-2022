import std/rdstdin
import std/strutils

let plays: array[3, array[3, int]] =
  # 0 - lose, 1 - draw, 2 - win
  # X  Y  Z
  [[1, 2, 0], # A
   [0, 1, 2], # B
   [2, 0, 1]] # C

proc parsePlay(play: string): int =
  let selected = split(play, ' ')
  let opponent = case selected[0]:
    of "A": 0 # rock
    of "B": 1 # paper
    else:   2 # scissors
  let us = (opponent + (case selected[1]:
    of "X": 2 # lose
    of "Y": 0 # draw
    else:   1 # win
  )) mod 3

  result =
    plays[opponent][us] * 3 + # win/lose * multiplier
    us + 1                    # played hand

var score: int
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok:
    break
  score += parsePlay(line)

echo(score)
