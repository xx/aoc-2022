import std/rdstdin
import std/strutils
import std/tables

var line: string
var cwd: string
var fs = initTable[string, int]()

while true:
  let ok = readLineFromStdin("", line)
  if not ok: break
  let argv = line.split(" ")
  case argv[0]:
    of "$":
      case argv[1]:
        of "cd":
          case argv[2]:
            of "/":
              cwd = "/"
            of "..":
              cwd = cwd[0..cwd.len - 2].rsplit("/", maxsplit=1)[0] & "/"
              if cwd == "":
                cwd = "/"
            else:
              cwd = cwd & argv[2] & "/"
          if not fs.hasKey(cwd):
            fs[cwd] = 0
        of "ls":
          discard
        else:
          echo("Unknown command: " & argv[1])
    of "dir":
      discard
    else:
      let size = parseInt(argv[0])
      if not fs.hasKey(cwd):
        fs[cwd] = size
      else:
        fs[cwd] += size
      if cwd != "/":
        var parent = cwd.rsplit("/", maxsplit=2)[0]
        while parent != "":
          fs[parent & "/"] += size
          parent = parent.rsplit("/", maxsplit=1)[0]
        fs["/"] += size

let needed_space = 30000000
let free_space = 70000000 - fs["/"]
var candidate = fs["/"]

for path, size in fs:
  if path == "/": continue
  if free_space + size > needed_space and size < candidate:
    candidate = size

echo(candidate)
