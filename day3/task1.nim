import std/rdstdin
import std/sequtils
import std/sets
import strutils

var matching = newseq[char](0)
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok:
    break
  matching.add(
    toSeq(
      toHashSet(line[0 .. (line.len div 2) - 1]) * # intersection
      toHashSet(line[(line.len div 2) .. line.len - 1])
    )
  )

var score: int
for item in matching:
  if item.isLowerAscii:
    score += item.int - 96
  else:
    score += item.int - 38

echo(score)
