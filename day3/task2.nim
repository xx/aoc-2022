import std/rdstdin
import std/sequtils
import std/sets
import strutils

var matching = newseq[char](0)

proc readThreeLines(): array[3, string] =
  for i in 0 .. 2:
    let ok = readLineFromStdin("", result[i])
    if not ok: break

while true:
  let lines = readThreeLines()
  if lines[0] == "":
    break
  matching.add(
    toSeq( # intersection of three lines
      toHashSet(lines[0]) *
      toHashSet(lines[1]) *
      toHashSet(lines[2])
    )
  )

var score: int
for item in matching:
  if item.isLowerAscii:
    score += item.int - 96
  else:
    score += item.int - 38

echo(score)
