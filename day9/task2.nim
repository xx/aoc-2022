import rdstdin
import strutils
import sets


const NUM_TAILS = 9


type
  Coords = tuple
    x, y: int


proc diff(a, b: int): int =
  if a > b: a - b
  else: b - a


var line: string
var tails: array[0 .. NUM_TAILS, Coords] # head + tails
var moved: array[0 .. NUM_TAILS, bool]
var visited = newSeq[Coords](0)


moved[0] = true # the head always moves

while true:
  let ok = readLineFromStdin("", line)
  if not ok: break

  let cmd = line.split(" ")
  for i in 1 .. parseInt(cmd[1]):
    case cmd[0]:
      of "U": tails[0].y.inc
      of "D": tails[0].y.dec
      of "R": tails[0].x.inc
      of "L": tails[0].x.dec

    for i in 1 .. NUM_TAILS:
      moved[i] = false
      if not moved[i - 1]:
        continue

      let head = tails[i - 1]
      let x_diff = diff(head.x, tails[i].x)
      let y_diff = diff(head.y, tails[i].y)

      if x_diff + y_diff == 3:
        # we are diagonal, yank
        if x_diff == 2:
          tails[i].y += head.y - tails[i].y
        else:
          tails[i].x += head.x - tails[i].x
        moved[i] = true

      if x_diff > 1:
        if head.x > tails[i].x: tails[i].x.inc
        else: tails[i].x.dec
        moved[i] = true

      if y_diff > 1:
        if head.y > tails[i].y: tails[i].y.inc
        else: tails[i].y.dec
        moved[i] = true

    visited.add(tails[^1])


echo(visited.toHashSet.card)
