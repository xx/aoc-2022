import rdstdin
import sequtils
import strutils

var grid = newSeq[seq[int]](0)
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok: break
  grid.add(line.mapIt(parseInt($it)))

var candidate: int

for x in 0 .. grid[0].len - 1:
  for y in 0 .. grid.len - 1:
    let tree = grid[x][y]
    var a, b, c, d: int
    for c_x in countdown(x - 1, 0):
      a.inc
      if grid[c_x][y] >= tree: break 
    for c_x in x + 1 .. grid.len - 1:
      b.inc
      if grid[c_x][y] >= tree: break 
    for c_y in countdown(y - 1, 0):
      c.inc
      if grid[x][c_y] >= tree: break 
    for c_y in y + 1 .. grid[x].len - 1:
      d.inc
      if grid[x][c_y] >= tree: break
    let score = a * b * c * d
    if score > candidate:
      candidate = score

echo(candidate)
