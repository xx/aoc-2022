import rdstdin
import sequtils
import strutils

var grid = newSeq[seq[int]](0)
var line: string

while true:
  let ok = readLineFromStdin("", line)
  if not ok: break
  grid.add(line.mapIt(parseInt($it)))

var visible_count = grid.len * 2 + grid[0].len * 2 - 4

for x in 1 .. grid[0].len - 2:
  for y in 1 .. grid.len - 2:
    let tree = grid[x][y]
    var visible = [true, true, true, true]
    for c_x in 0 .. x - 1:
      if grid[c_x][y] >= tree:
        visible[0] = false
        break
    for c_x in x + 1 .. grid.len - 1:
      if grid[c_x][y] >= tree:
        visible[1] = false
        break
    for c_y in 0 .. y - 1:
      if grid[x][c_y] >= tree:
        visible[2] = false
        break
    for c_y in y + 1 .. grid[x].len - 1:
      if grid[x][c_y] >= tree:
        visible[3] = false
        break
    if visible.anyIt(it):
      visible_count.inc

echo(visible_count)
